#!/bin/bash

# Convert a normal bare git repo in to one shared with a group.
# Before running this tool make sure the foo.git directory is owned by the group you'd like to share with.
# Michael Fincham <michael@hotplate.co.nz> 2017-07-04

if [[ ! -d $1 ]]; then
	echo "error: you need to point this tool at a bare git repo (which is owned by the group to share with)"
	exit 1
fi

group=$(stat -c '%G' "$1")
cd "$1"

if git rev-parse --is-bare-repository >/dev/null; then
	echo "Sharing \`$1' with \`$group'..."
	chmod -R g+rw ./
	find ./ -type d -exec chmod g+s "{}" \;
	git init --bare --shared=all
else
	echo "error: it doesn't look like \`$1' is a bare git repo"
	exit 1
fi
